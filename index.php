<?php

require 'vendor/autoload.php';

// Deberian colocarse en un archivo separado del repositorio
define("SPOTIFY_CLIENT_ID", "8af363cb09ff43528b4438432cdd49ce");
define("SPOTIFY_SECRET_ID", "d97f4b748d67487ba964a0da2f297d9a");

// Create and configure Slim app
use Slim\App;

class SpotifyConnector {

    private $client;

    public function __construct()
    {
        $this->client = new GuzzleHttp\Client();
    }

    public function getArtists($band_name) {
        $params = [
            'q' => $band_name,
            'type' => 'artist'
        ];

        $url = 'https://api.spotify.com/v1/search';

        $response = $this->getDataFromSpotify($url, $params);

        return $response->artists->items;
    }

    public function getAlbums($band_id) {
        $albums = [];
        $has_more = true;
        $url = 'https://api.spotify.com/v1/artists/' . $band_id . '/albums?include_groups=album%2Csingle&limit=50';

        while ($has_more) {
            $response = $this->getDataFromSpotify($url);

            foreach ($response->items as $album) {
                $album_aux = [
                    'name' => $album->name,
                    'released' => $album->release_date,
                    'tracks' => $album->total_tracks,
                ];

                foreach ($album->artists as $artist) {
                    $album_aux['artists'][] = $artist->name;
                }

                foreach ($album->images as $image) {
                    if ($image->height === 640) {
                        $album_aux['cover'] = ['height' => $image->height, 'width' => $image->width, 'url' => $image->url];
                    }
                }

                $albums[] = $album_aux;
            }

            $url = $response->next;

            if(strcmp($response->next,'') === 0) {
                break;
            }
        }

        return $albums;
    }

    private function getDataFromSpotify($url, $params = null) {
        $token = $this->getToken();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ];

        $res = $this->client->request('GET', $url, [
            'headers' => $headers,
            'query' => $params
        ]);

        return json_decode($res->getBody()->getContents());
    }

    private function getToken() {
        $client_id = SPOTIFY_CLIENT_ID;
        $client_secret = SPOTIFY_SECRET_ID;
        $token_base_64 = base64_encode($client_id . ':' . $client_secret);

        $headers = [
            'Authorization' => 'Basic ' . $token_base_64,
            'Accept' => 'application/json',
            'Content-Type' => 'application/x-www-form-urlencoded',
        ];

        $params = [
            'grant_type' => 'client_credentials'
        ];

        try {
            $res = $this->client->request('POST', 'https://accounts.spotify.com/api/token', [
                'headers' => $headers,
                'query' => $params
            ]);
        } catch (Exception $e) {

        }

        $response = json_decode($res->getBody()->getContents());

        return $response->access_token;
    }
}

$config = ['settings' => [
    'addContentLengthHeader' => false,
]];
$app = new App($config);

// Define app routes
$app->get('/', function ($request, $response, $args) {
    $errors[] = ['error' => 'Unico endpoint activo: /api/v1/albums?q=<band-name>'];
    return $response->withJson($errors);
});

$app->get('/api/v1/albums', function ($request, $response, $args) {
    // Validar que se ingrese el nombre de la banda
    if (!$request->getQueryParam('q') || strcmp($request->getQueryParam('q'), '') === 0) {
        $errors[] = ['error' => 'Debe ingresar el nombre de la banda'];
        return $response->withJson($errors);
    }

    $band_name = $request->getQueryParam('q');

    $spotifyConnector = new SpotifyConnector();

    // Obtener id de la banda
    $artists = $spotifyConnector->getArtists($band_name);

    // No se encontro el artista
    if (count($artists) < 1) {
        $errors[] = ['error' => 'No se encontraron albums para la banda: ' . $band_name];
        return $response->withJson($errors);
    }

    // Varios resultados
    $band_id = null;
    if (count($artists) > 1) {
        $artist_names = [];

        foreach ($artists as $artist) {
            $artist_names[] = $artist->name;

            if(strcmp(strtolower($artist->name), strtolower($band_name)) === 0 ) {
                $band_id = $artist->id;
                break;
            }
        }

        if (!$band_id) {
            $errors[] = [
                'error' => 'Se encontraron multiples resultados para la banda: ' . $band_name,
                'bandas' => $artist_names
            ];
            return $response->withJson($errors);
        }
    }

    // Buscar y devolver los albums
    if (!$band_id) {
        $band_id = $artists[0]->id;
    }

    $albums = $spotifyConnector->getAlbums($band_id);

    return $response->withJson($albums);
});

// Run app
$app->run();