FROM php:7.2-apache

RUN apt-get update && apt-get upgrade -y

RUN apt-get install apt-utils nano git libpng-dev -y

RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli
RUN docker-php-ext-install gd
RUN docker-php-ext-install bcmath

RUN a2enmod rewrite

COPY . /var/www/html
WORKDIR /var/www/html

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
